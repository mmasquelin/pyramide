#!/bin/bash
# Creation d'une pyramide en bash sur stdout
# Credits: http://technicalworldforyou.blogspot.com/2013/08/shell-script-to-print-pyramid-of-stars.html

makePyramid()
{
  # Parametre $1 que l'on passe a la fonction (exemple: 5)
  n=$1;

  # Boucle pour les lignes
  for((i=1;i<=n;i++))
  do

      # Boucle pour les espaces
      for((k=i;k<=n;k++))
      do
        echo -ne " ";
      done

      # Boucle qui affiche la partie 1 et 2 de la pyramide
      for((j=1;j<=2*i-1;j++))
      do
	echo -ne "*"
      done
      
      # Cet appel à echo provoque l'affichage d'une nouvelle ligne
      echo;
  done
}

# Appel de la fonction creee (main)

# Changez la valeur par ce que vous souhaitez
makePyramid $1 
